package com.jbr440.customerinvoiceapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InvoiceController {
    @CrossOrigin
    @GetMapping("/invoices")
    public static ArrayList<Invoice> getInvoiceList() {
        Customer customer1 = new Customer(11, "Nam", 15);
        Customer customer2 = new Customer(12, "Hoa", 10);
        Customer customer3 = new Customer(13, "Lan", 20);
        System.out.println("Customer1 la: " + customer1);
        System.out.println("Customer2 la: " + customer2);
        System.out.println("Customer3 la: " + customer3);
        Invoice invoice1 = new Invoice(21, customer1, 9000.0);
        Invoice invoice2 = new Invoice(22, customer2, 11000.0);
        Invoice invoice3 = new Invoice(23, customer3, 7000.0);
        System.out.println("Invoice1 la: " + invoice1);
        System.out.println("Invoice2 la: " + invoice2);
        System.out.println("Invoice3 la: " + invoice3);
        ArrayList<Invoice> invoiceList = new ArrayList<Invoice>();
        invoiceList.add(invoice1);
        invoiceList.add(invoice2);
        invoiceList.add(invoice3);
        return invoiceList;
    }
}
